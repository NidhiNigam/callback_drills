
// Q1. Create a new file data.json

var fs = require('fs');
var path = require('path');
var data = {
    max: {
        colors: ['orange', 'Red']
    }
}
fs.writeFile(path.join(__dirname, "./data.json"), JSON.stringify(data), function(err) {
    if (err) {
        console.log('error', err);
    }
});


// // 2. Copy the contents of the data.json into a new folder output/data.json and delete the original file.


fs.readFile(path.join(__dirname, "./data.json"), function (err, data) {
    if (err) {
        console.log(err);
    }
    else {
        let nData = JSON.parse(data);

        fs.writeFile(path.join(__dirname, "./output/data.json"), JSON.stringify(nData),function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log("Data added successfully");
            }
        });
    }
});

//  {
//     fs.unlink('data.json', function (err) {
//         if (err) throw err;
//         console.log('File deleted successfully!');
//     });
// };


// // 3. Write a function that takes a person name and fav color as parameters and writes to data.json file
// //     (Do not replace the old content).

let addNameAndColor=(name, favcolors) => {
    fs.readFile(path.join(__dirname, "./output/data.json"), function(err, data) {
        if (err) {
            console.log(err);
        }
    });
     data[name] = { colors: favcolors };
    return data;

}
fs.readFile(path.join(__dirname, "./output/data.json"), function(err, data) {
    if (err) {
        console.log(err);
    }
    else {

        const nData = addNameAndColor("Nidhi", ["Pink", "Black"]);
        fs.writeFile(path.join(__dirname, "./output/data.json"), JSON.stringify(nData), function (err) {
            if (err) {
                console.log(err);
            }
            else {
                console.log("Name and color added successfully");
            }
        });
    }
});

// //  4. Write a function that takes a person name and fav hobby as param and add that hobby as a separate key and write to data.json.
// //     (Do not replace the old content).

let addNameAndHobby=(name, favhobbies)=> {
    fs.readFile(path.join(__dirname, "./output/data.json"), function(err, data) {
        if (err) {
            console.log(err);
        }
        });

    if (data[name]) {
        data[name].hobbies = favhobbies;
    }
    else {
        data[name] = { hobbies: favhobbies };
    }
    return data;
}

fs.readFile(path.join(__dirname, "./output/data.json"), function(err, data){
    if (err) {
        console.log(err);
    }
    else {

        const newData = addNameAndHobby("Nidhi", ["Eating", "Dancing"]);
        fs.writeFile(path.join(__dirname, "./output/data.json"), JSON.stringify(newData), function(err) {
            if (err) {
                console.log(err);
            }
            else {
                console.log("Name and hobbies added successfully");
            }
        })
    }
});